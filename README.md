**Installation**

By using **CocoaPods**.

Step 1: Just add the following line to your pod file

pod 'NetworkHelper', :git => 'https://VenkatVarra@bitbucket.org/VenkatVarra/networkhelper.git'

Step 2: pod update

Step 3 : import NetworkHelper module in your classes

By using **Drag and Drop**

Step 1: First clone the project.open the terminal type the following the command

git clone https://VenkatVarra@bitbucket.org/VenkatVarra/networkhelper.git

Step 2 : drag and drop the NetworkHelper.xcodeproj file into your project

Step 3: Add NetworkHelper.frameworkiOS in embedded libraries

Step 4 : import NetworkHelper module in your classes

**Usage**

**Login** 

        let input = ["Username":"your username","Passwor":"********"]
        NetworkHelper.sharedInstance.login(input: input)

 In the same way you can use other API calls



You can create your own framework and pod creation for your framework. Please following this [article](http://www.colorssoftware.com/blog/how-to-create-native-ios-sdk-framework-library-to-third-party-developers-to-develop-apps-using-web-servicesapi-backend/)